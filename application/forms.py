from django import forms
from application.models import Profile


class UserPanelForm(forms.ModelForm):
    image_link = forms.CharField(required=False, widget=forms.TextInput(
        attrs={
            'placeholder': 'Link to profile photo',
            'class': 'form-control'
    }))
    description = forms.CharField(required=False, widget=forms.Textarea(
        attrs={
            'rows': '4',
            'cols': '40',
            'placeholder': 'About me',
            'class': 'form-control'
        }))

    class Meta:
        model = Profile
        fields = ('image_link', 'description',)
