from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate, logout as auth_logout

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            passwd = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=passwd)
            login(request, user)
            return redirect('index')
    else:
        form = UserCreationForm()
    return render(request, 'authentication/register.html', {'form': form})

def logout(request):
    auth_logout(request)
    return redirect('index')

